def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


def game_over(board, num):
    print_board(board)
    print("GAME OVER")
    print(board[num], "has won")
    exit()

# row 1 = 0, 1, 2
# row 2 = 3, 4, 5
# row 3 = 6, 7, 8
def is_row_winner(board, row_number):
    if board[3 * row_number - 3] == board[(3 * row_number - 3) + 1] and board[(3 * row_number - 3) + 1] == board[(3 * row_number - 3) + 2]:
        return True

# column 1 = 0, 3, 6
# column 2 = 1, 4, 7
# column 3 = 2, 5, 8
def is_column_winner(board, column_number):
    if board[column_number - 1] == board[(column_number) + 2] and board[column_number + 2] == board[column_number + 5]:
        return True

# diagonal 1 = 0, 4, 8
# diagonal 2 = 2, 4, 6
def is_diagonal_winner(board, diagonal_number):
    if board[(diagonal_number * 2) - 2] == board[4] and board[4] == board[10 - (diagonal_number * 2)]:
        return True


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    for i in range(1, 4):
        if is_row_winner(board, i):
            game_over(board, (i * 3) - 3)
        elif is_column_winner(board, i):
            game_over(board, i - 1)


    # if is_row_winner(board, 1):
    #    game_over(board, 0)
    # elif is_row_winner(board, 2):
    #     game_over(board, 3)
    # elif is_row_winner(board, 3):
    #     game_over(board, 6)
    # elif is_column_winner(board, 1):
    #     game_over(board, 0)
    # elif is_column_winner(board, 2):
    #     game_over(board, 1)
    # elif is_column_winner(board, 3):
    #     game_over(board, 2)
    if is_diagonal_winner(board, 1):
        game_over(board, 0)
    elif is_diagonal_winner(board, 2):
        game_over(board, 2)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
